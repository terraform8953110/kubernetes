# Define Local Values in Terraform

locals {
  # Define a local variable 'owners' which gets its value from the 'business_division' variable.
  owners = var.business_division

  # Define a local variable 'environment' which gets its value from the 'environment' variable.
  environment = var.environment

  # Define a local variable 'name' which concatenates the values of 'business_division' and 'environment'.
  name = "${var.business_division}-${var.environment}"

  # Define a map variable 'common_tags' containing common tags for resources.
  common_tags = {
    owners      = local.owners
    environment = local.environment
  }
}
