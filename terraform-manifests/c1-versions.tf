# Terraform configuration specifying required versions and providers for managing AWS infrastructure.

# Specify the required version of Terraform.
terraform {
  required_version = ">= 1.7"
  # Ensure Terraform version 1.7 or later is used.

  # Specify required providers and their versions.
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.0"
      # Use AWS provider version 5.0 or later.
    }
  }
}

# Configure the AWS provider.
provider "aws" {
  region = var.aws_region
  # Specify the AWS region where resources will be provisioned.

  # Note: Make sure to set up AWS credentials or IAM role for Terraform to use.
}
